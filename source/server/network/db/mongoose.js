const mongoose = require('mongoose');
const config = require('../../../data/config.json');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/news_site`);

module.exports = { mongoose };
