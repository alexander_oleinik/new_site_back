// const { mongoose } = require('../../db/mongoose');
const User = require('../models/userModel');

module.exports = {
  getAllUsers(req, res) {
    User.find().then(users => {
    //   console.log(users);
      res.send(users);
    });
  },

  getTokenUser(req, res, next) {
    const userProps = req.body;
    const UserEmail = req.params.email;

    User.findOne({ email: UserEmail }, userProps)
      .then(user => res.send(user.token))
      .catch(next);
  },

  authUser(req, res, next) {
    const userToken = req.params.token;
    const userEmail = req.params.email;

    User.findOneAndUpdate({ email: userEmail }, { token: userToken })
      .then(() => res.send('User auth success'))
      .catch(next);
  },
};
