// const { mongoose } = require('../../db/mongoose');
const News = require('../models/newsModel');

module.exports = {
  getNews(req, res) {
    News.find().then(news => {
      // console.log(news)
      res.send(news);
    });
  },

  createNews(req, res) {
    const newsProps = req.body;

    const news = new News({
      image_url: newsProps.image_url,
      title: newsProps.title,
      description: newsProps.description,
      date: new Date(),
      members: newsProps.members,
      type: newsProps.type,
    });

    news.save().then(() => {
      res.send('news created');
    });
  },

  editNews(req, res, next) {
    const newsId = req.params.id;
    const newsProps = req.body;

    News.findByIdAndUpdate({ _id: newsId }, newsProps)
      .then(() => News.findById({ _id: newsId }))
      .then(news => res.send(news))
      .catch(next);
  },

  deleteNews(req, res, next) {
    const newsId = req.param.id;

    News.findByIdAndRemove({ _id: newsId })
      .then(news => res.status(204).send(news))
      .catch(next);
  },
};
