const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NewsSchema = new Schema({
  image_url: String,
  title: String,
  description: String,
  date: Date,
  owner_id: String, // user token
  members: [],
  type: String,
});

const News = mongoose.model('news', NewsSchema);

module.exports = News;
