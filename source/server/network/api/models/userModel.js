const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// const News = require('./newsModel')

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
  },

  role: {
    type: Number,
    default: 1,
  },

  token: {
    type: String,
  },
});

const User = mongoose.model('user', UserSchema);
module.exports = User;
