const newsController = require('./controllers/newsController');
const userController = require('./controllers/userController');

module.exports = app => {
  // News API
  app.get('/api', newsController.getNews);

  app.post('/api/news/create', newsController.createNews);

  app.put('/api/news/:id', newsController.editNews);

  app.delete('api/news/:id', newsController.deleteNews);

  // Users API

  app.post('/api/user', userController.getAllUsers);

  app.post('/api/user/:email', userController.getTokenUser);

  app.post('api/user/:email', userController.authUser);

  // Upload Img

  // app.put('')
};
