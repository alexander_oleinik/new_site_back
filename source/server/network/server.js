const { createServer } = require('http');
const express = require('express');
const bodyParser = require('body-parser');
// const ip = require("ip");
const { resolve } = require('path');
const config = require('./../../data/config.json');
const routes = require('./api/routes');

class Server {
  constructor() {
    this.app = express();
    this.server = createServer(this.app);
    // this.host = ip.address();
    this.routes = routes;
    this.host = 'localhost';
    this.port = config.server.port || 3000;
    this.init();
  }

  async init() {
    await this.start();
    this.initMiddleware();
    this.initView();
    this.initGetHandlers();
  }

  async start() {
    try {
      await this.server.listen(this.port, this.host);
      const message = ` Server ${this.host} is listening on port ${this.port}`;
      global.console.log(new Date() + message);
    } catch (e) {
      global.console.log(e);
    }
  }

  initMiddleware() {
    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Credentials', true);
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
      res.header('Access-Control-Allow-Headers', 'Cache-Control');
      next();
    });

    this.app.use(bodyParser.json());
  }

  initView() {
    this.app.use('/public', express.static('public'));
  }

  initGetHandlers() {
    this.app.get('/favicon.ico', (req, res) => res.sendStatus(204));

    // init router from './api/routes'
    this.routes(this.app);

    this.app.get('/', (req, res) => {
      res.sendFile(resolve('./public/index.html'));
    });
  }
}

module.exports = Server;
